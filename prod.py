from main import app


@app.route('/version')
def version():
    return open('version').read()
