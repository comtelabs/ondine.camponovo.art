import random
from PIL import Image, ImageDraw, ImageFont


class CheckCode:
    def __init__(self, id, width=235, height=50, code_count=5, font_size=45, point_count=50, line_count=10,
                 img_format='png'):
        """
        Can generate a picture of a random verification code after noise reduction
                 :param id: id of code
                 :param Width: Picture Width Unit PX
                 :param HEIGHT: Picture Height Unit PX
                 :param code_count: Verification code number
                 :param font_size: Font size
                 :param Point_count: Noise
                 :param line_count: Rowed number
                 :param img_format: Picture Format
                 :return generated image of the Bytes type of DATA
        """
        self.id = id
        self.width = width
        self.height = height
        self.code_count = code_count
        self.font_size = font_size
        self.point_count = point_count
        self.line_count = line_count
        self.img_format = img_format

    @staticmethod
    def getRandomColor():
        """Get random colors (R, G, B)"""
        return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)

    @staticmethod
    def getRandomStr():
        """Get a random string, and the color of each character is also random"""
        random_num = str(random.randint(0, 9))
        random_low_alpha = chr(random.randint(97, 122))
        random_upper_alpha = chr(random.randint(65, 90))
        random_char = random.choice([random_num, random_low_alpha, random_upper_alpha])
        return random_char

    def getValidCodeImg(self):
        image = Image.new('RGB', (self.width, self.height), (255, 255, 255))
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype('static/Manuscripte-Regular.ttf', size=self.font_size)

        temp = []
        for i in range(self.code_count):
            random_char = self.getRandomStr()
            draw.text((25 + i * 40, 4), random_char, self.getRandomColor(), font=font)
            temp.append(random_char)
        valid_str = ''.join(temp)

        for i in range(self.line_count):
            x1 = random.randint(0, self.width)
            x2 = random.randint(0, self.width)
            y1 = random.randint(0, self.height)
            y2 = random.randint(0, self.height)
            draw.line((x1, y1, x2, y2), fill=self.getRandomColor())

        for i in range(self.point_count):
            draw.point([random.randint(0, self.width), random.randint(0, self.height)], fill=self.getRandomColor())
            x = random.randint(0, self.width)
            y = random.randint(0, self.height)
            draw.arc((x, y, x + 4, y + 4), 0, 90, fill=self.getRandomColor())

        image.save(f'static/cache/{self.id}.png', self.img_format)
        return valid_str, f'/static/cache/{self.id}.png'
