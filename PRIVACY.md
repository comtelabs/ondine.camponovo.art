# Mentions légales
Voici LA page sérieuse de ce site, si tu t'es perdu fais vite retour arrière, sinon garde le smile !

<p style="text-align: center;">
    <img style="width: 25%; margin: auto;" alt="Ondine Dessin Couper Cheveux" src="/static/assets/mentions-legales.png">
</p>

## Identité
- Nom du site web : Ondine Camponovo
- Adresse : [https://ondine.camponovo.art](https://ondine.camponovo.art)
- Propriétaire et responsable de publication : Ondine Camponovo
- Conception : [ComteLabs](https://www.comtelabs.fr/) & [CAMARM-DEV](https://www.camarm.dev/)
- Réalisation : [CAMARM-DEV](https://www.camarm.dev/)
- Graphisme et photographie : Ondine Camponovo
- Hébergement : [OVH](https://ovh.com/) France<br>
## Conditions d’utilisation
L’utilisation du présent site implique l’acceptation pleine et entière des conditions générales d’utilisation décrites ci-après. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment.

## Informations
Les informations et documents du site sont présentés à titre indicatif, n’ont pas de caractère exhaustif, et ne peuvent engager la responsabilité du propriétaire du site.

Le propriétaire du site ne peut être tenu responsable des dommages directs et indirects consécutifs à l’accès au site.

## Consultation et confidentialité
Les utilisateurs du site peuvent librement accéder au contenu et le référencer.

Aucune information à caractère personnel n'est collectée lors de la navigation, ni aucun cookie utilisé ou stocké.
Le site ne recueillant pas d’informations personnelles, il n’est pas assujetti à une déclaration à la CNIL.

## Propriété intellectuelle
Tous les éléments accessibles sur le site (textes, images, graphismes, logo, icônes, gif, photos, sons, logiciels, etc.) restent la propriété exclusive de leurs auteurs, en ce qui concerne les droits de propriété intellectuelle ou les droits d’usage.

Toute reproduction, représentation, publication de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est autorisée sous réserve de respect des dispositions décrites ci-dessous (cf. Licence et crédits). La modification ou l'adaptation de tout élément n'est pas autorisée, sauf mention écrite expresse de la part de ses auteurs.

Toute exploitation non autorisée du site ou de l’un des éléments qu’il contient est considérée comme constitutive d’une contrefaçon et poursuivie.

Les marques et logos reproduits sur le site sont déposés par les sociétés qui en sont propriétaires.

## Licences et crédits
Les textes, images, gif et photos présentés sur le site ont été créés, sauf mention contraire, par Ondine Camponovo qui en autorise la libre utilisation sous la [licence Creative Commons CC BY-NC-ND 3.0 FR <img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="CC Logo" height="50" width="100" src="https://mirrors.creativecommons.org/presskit/logos/cc.logo.large.png">](https://creativecommons.org/licenses/by-nc-nd/3.0/fr/). Si vous souhaitez reproduire tout ou partie de ces productions, vous devez créditer son autrice, ne pas en faire d'usage commercial, ni de modification.

La police d'écriture "Manuscripte" utilisée tout au long des pages de ce site web ([Manuscripte.ttf](/static/Manuscripte-Regular.ttf)) est une création originale d'Ondine Camponovo qui en autorise la libre utilisation également sous la licence [licence Creative Commons CC BY-NC-ND 3.0 FR <img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="CC Logo" height="50" width="100" src="https://mirrors.creativecommons.org/presskit/logos/cc.logo.large.png">](https://creativecommons.org/licenses/by-nc-nd/3.0/fr/).

Le site web a été développé par CAMARM-DEV, son code source est ouvert sous [licence CeCILLv2.1](http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.html) et consultable sur [GitLab](https://gitlab.com/comtelabs/ondine.camponovo.art).
Ce site a été développé en [Python](https://python.org)[&nbsp;<img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="Python Logo" height="15" src="https://python.org/favicon.ico" width="15"/>](https://python.org) et utilise les composants et librairies suivantes :

- [Flask](https://flask.palletsprojects.com)[&nbsp;<img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="Flask Logo" height="15" src="https://flask.palletsprojects.com/en/2.0.x/_static/flask-icon.png" width="15"/>](https://flask.palletsprojects.com)
- [Bulma](https://bulma.io)[&nbsp;<img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="Bulma Logo" height="15" src="https://bulma.io/favicons/favicon.ico?v=201701041855" width="15"/>](https://bulma.io)
- [MongoDB](https://mongodb.com)[&nbsp;<img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="MongoDB Logo" height="15" src="https://mongodb.com/favicon.ico" width="15"/>](https://mongodb.com)
- [Markdown](https://pypi.org/project/Markdown) [&nbsp;<img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="Markdown Logo" height="15" src="https://www.markdownguide.org/favicon.ico" width="15"/>](https://python-markdown.github.io/)

Les présentes mentions légales sont librement adaptées du modèle fourni par [WebExpress dans sa version 2.0 <img style="margin-top: auto; margin-bottom: auto; height: 90%;" alt="CC Logo" height="50" width="100" src="https://www.webexpress.fr/wp-content/uploads/2015/09/webexpress-logo-v3-vecto.svg">](https://www.webexpress.fr/contact/mentions-legales/).

## Liens
### Liens sortants
Le propriétaire du site décline toute responsabilité et n’est pas engagé par le référencement via des liens hypertextes, de ressources tierces présentes sur le réseau Internet, tant en ce qui concerne leur contenu que leur pertinence.

### Liens entrants
Le propriétaire du site autorise les liens hypertextes vers l’une des pages de ce site, à condition que ceux-ci ouvrent une nouvelle fenêtre et soient présentés de manière non équivoque afin d’éviter :

- tout risque de confusion entre le site citant et la propriétaire du site ;
- ainsi que toute présentation tendancieuse, ou contraire aux lois en vigueur.

La propriétaire du site se réserve le droit de demander la suppression d’un lien s’il estime que le site source ne respecte pas les règles ainsi définies.

## Textes légaux supports des présentes mentions légales
- Articles L111-1 et suivants du Code de la Propriété Intellectuelle du 1er juillet 1992
- Article 41 de la loi du 11 mars 1957
- Article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995
- Articles L.335-2 et suivants du Code de Propriété Intellectuelle
- Loi n° 78-87 du 6 janvier 1978, modifiée par la loi n° 2004-801 du 6 août 2004, relative à l’informatique, aux fichiers et aux libertés
- Articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés
- Loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données
- Loi n° 2004-801 du 6 août 2004
- Article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique

<a href="/PRIVACY.md">PRIVACY.md</a>
<style>
    ul li {
        list-style-image: url("/static/assets/list-style.png") !important;
        list-style-position: inside;
    }
    ul li::marker {
        transform: scale(0.5);
    }
</style>
