FROM python:3-alpine
WORKDIR /website
COPY . .
RUN pip install pip --upgrade
RUN pip install -r requirements.txt
RUN mkdir -p uploads
RUN mkdir -p static/cache
CMD ["gunicorn", "--bind", "0.0.0.0:1210", "prod:app"]
EXPOSE 1210