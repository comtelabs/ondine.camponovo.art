from typing import List, Tuple
from hashlib import scrypt, sha256
from email.message import EmailMessage
import datetime
import os
import random
import secrets
import smtplib
import markdown
import pymongo
from bson import ObjectId
from flask import Flask, render_template, request, make_response, redirect, send_file, jsonify
from env_loader import load_env

from captcha import CheckCode

app = Flask('Ondine Website')
env = load_env('.')
args = '?retryWrites=true&w=majority'
MONGO_URL = env.get('MONGO_URL')
MAIL_PASSWORD = env.get('NOREPLY_PASSWORD')
client = pymongo.MongoClient(MONGO_URL + args)
creations = client.Ondidine.creations
categories = client.Ondidine.categories
registeredUsers = client.Ondidine.users
canBeSearchedTags = client.Ondidine.search_tags
token_active = ''
ctokens = {}


def isNotTagInDB(tag: str) -> bool:
    """
    Is there not the tag in db ?
    :param tag: tag
    :return: bool
    """
    return canBeSearchedTags.find_one({'name': tag}) is None


def noFilters() -> bool:
    """
    Is there no filters query in args ?
    :return: bool
    """
    return request.args.get('filters') is None


def searchQuery() -> bool:
    """
    Is there a search query in args ?
    :return: bool
    """
    return request.args.get('search') is not None


def generateRandomToken() -> str:
    """
    Generate a random tokens
    :return: str
    """
    return secrets.token_hex(64)


def getConnectedAdministrator() -> Tuple[bool, str]:
    """
    Get current connected administrator and if credentials are rights
    :return: Tuple(bool, str)
    """
    cookie_token = request.cookies.get('token')
    for user, token in tokens.items():
        if token == cookie_token:
            return True, user
    return False, 'null'


def validConnectionToken() -> bool:
    """
    Cookie token is valid ?
    :return: bool
    """
    return getConnectedAdministrator()[0]


def salt(name) -> str:
    """
    Get salt of hash
    :param name: name to salt
    :return:
    """
    new_name = ''
    for letter in name:
        new_name = letter + new_name
    return str(sha256(new_name.encode()).hexdigest())


def hashPassword(text, key) -> bytes:
    """
    Hash a text with key
    :param text: text
    :param key: key
    :return: bytes
    """
    return scrypt(password=text.encode(), salt=key.encode(), n=2, r=64, p=4)


def isRight(password: str, name: str) -> bool:
    """
    Is password Right ?
    :param password: password of user
    :param name: name of user
    :return: bool
    """
    return hashPassword(password, salt(name)) == users[name]


def sendMail(subject: str, name: str, email: str, content: str, to: str = 'ondine@camponovo.art') -> bool:
    """
    Send a mail
    :param subject: subject of mail
    :param name: name of sender
    :param email: email of sender
    :param content: content of mail
    :return: bool
    """
    try:
        server = smtplib.SMTP('ns0.ovh.net', 5025)
        server.set_debuglevel(1)
        server.login("noreply@camponovo.art", MAIL_PASSWORD)
        msg = EmailMessage()
        msg.set_content(f"{content}\n\nEnvoyé depuis https://ondine.camponovo.art par {name} ({email})")
        msg['Subject'] = f'[ondine.camponovo.art] {subject}'
        msg['From'] = "Mon Site Web <noreply@camponovo.art>"
        msg['To'] = "Ondine <ondine@camponovo.art>"
        server.sendmail("noreply@camponovo.art", to, msg.as_string())
        server.quit()
    except Exception as error:
        print(f"Error when send email: {error.__class__.__name__}")
        return False
    else:
        print('Email sent correctly')
        return True


@app.route('/')
def home():
    """
    HTML home page
    :return: html page
    """
    return render_template('home.html')


@app.route('/more')
def more():
    """
    HTML more page
    :return: html page
    """
    return render_template('more.html')


@app.route('/pro')
def pro():
    """
    HTML pro page
    :return: html page
    """
    return render_template('pro.html')


@app.route('/contact')
def contact():
    """
    HTML contact page
    :return: html page
    """
    return render_template('contact.html')


@app.route('/privacy')
def privacy():
    """
    HTML privacy page
    :return: html page
    """
    return render_template('privacy.html', text=markdown.markdown(open('PRIVACY.md').read()))


@app.route('/static/gif/final/<gif>')
def reloadGifs(gif):
    """
    Reload gif when reload page
    :return: file, 200
    """
    return send_file(f'static/gif/final/{gif}'), 200


@app.route('/get-captcha')
def getCaptcha():
    """
    Create and save a random captcha image
    :return: json
    """
    global ctokens
    captcha_token = secrets.token_urlsafe(64)
    captcha = CheckCode(captcha_token)
    string, path = captcha.getValidCodeImg()
    ctokens[f'{captcha_token}'] = string
    print(string, path, captcha_token)
    return jsonify({'image': path, 'token': captcha_token})


@app.route('/send-mail', methods=['POST'])
def sendEmail():
    """
    Send email to ondine
    :return: html page
    """
    global ctokens
    form = request.form
    code = request.form.get('code')
    token = request.form.get('token')
    if ctokens[token] == code:
        del ctokens[token]
        subject = form['subject']
        name = form['name']
        email = form['email']
        content = form['content']
        sendMail(subject, name, email, content)
        return redirect('/?msg=Message%20envoyé%20!')
    return redirect('/?msg=Erreur%20d\'envoi%20!%20Êtes%20vous%20un%20robot%20?')


@app.route('/uploads/<file>')
def getUploadedFile(file: str):
    """
    Return files in uploads/
    :param file: file query
    :return: file
    """
    return send_file(f'uploads/{file}')


@app.route('/robots.txt')
def getRobotFile():
    """
    Return robots.txt file
    :return: file
    """
    return send_file('robots.txt')


@app.route('/favicon.ico')
def getFavicon():
    """
    Return favicon file
    :return: file
    """
    return send_file('static/assets/favicon.png')


@app.route('/PRIVACY.md')
def getPrivacyFile():
    """
    Return PRIVACY.md file
    :return: file
    """
    return send_file('PRIVACY.md')


@app.route('/creations')
def viewCreations():
    """
    View all creation, by filters, search and categories
    :return: html page
    """
    search = request.args.get('search')
    if noFilters():
        creationsCategories = [replaceIdByTag(category) for category in categories.find()]
        return render_template('cards.html', creations=creationsCategories, filters=['tout'], search=search)
    else:
        tags = request.args.get('filters').split(',')
        tags.remove('tout') if 'tout' in tags else ...
        if searchQuery():
            if len(tags) <= 0:
                results = creations.find({'$text': {'$search': search}}).sort('$natural', -1)
            else:
                results = creations.find({'$text': {'$search': search}, 'tags': {'$elemMatch': {'$eq': tag} for tag in tags}}).sort('$natural', -1)
        else:
            results = creations.find({'tags': {'$elemMatch': {'$eq': tag}} for tag in tags}).sort('$natural', -1)
        return render_template('cards.html', creations=results, filters=tags, search=search)


@app.route('/card/<cardId>')
def card(cardId: str):
    """
    Get html page with card information
    :param cardId: id of card
    :return: html page
    """
    creation = creations.find_one({'_id': ObjectId(cardId)})
    creation['content'] = markdown.markdown(creation['content'])
    return render_template('card.html', crea=creation)


@app.route('/admin', methods=['GET', 'POST'])
def admin():
    """
    Admin panel
    :return: html page
    """
    valid_token, user = getConnectedAdministrator()
    if valid_token:
        return render_template('panel.html', admin=user)
    elif request.method == 'GET':
        return render_template('admin.html')
    else:
        result = request.form
        password = result['password']
        name = result['name']
        if isRight(password, name):
            resp = make_response(render_template('panel.html', admin=name))
            token = tokens[name]
            resp.set_cookie('token', token)
            return resp
        else:
            return render_template('admin.html')


@app.route('/admin/add-card')
def addCardUI():
    """
    Add card html page
    :return: html page
    """
    if validConnectionToken():
        return render_template('add-card.html')
    else:
        return redirect('/admin')


@app.route('/admin/gallery')
def adminCardGallery():
    """
    Remove / update card gallery
    :return: html page
    """
    if validConnectionToken():
        search = request.args.get('search')
        if noFilters():
            creationsCategories = [replaceBaseurl(replaceIdByTag(category), '/admin/gallery?filters=') for category in categories.find()]
            return render_template('cards.html', creations=creationsCategories, filters=['tout'], search=search)
        else:
            tags = request.args.get('filters').split(',')
            tags.remove('tout') if 'tout' in tags else ...
            if searchQuery():
                if len(tags) <= 0:
                    results = creations.find({'$text': {'$search': search}})
                else:
                    results = creations.find({'$text': {'$search': search}, 'tags': {'$elemMatch': {'$eq': tag} for tag in tags}})
            else:
                results = creations.find({'tags': {'$elemMatch': {'$eq': tag}} for tag in tags})
            results = [replaceBaseurl(result, '/admin/modify/') for result in results]
            return render_template('cards.html', creations=results, filters=tags, search=search)
    else:
        return redirect('/admin')


@app.route('/admin/lutina', methods=['POST'])
def lutinaArmand():
    """
    Send mail to developer
    :return: redirect to /admin
    """
    if validConnectionToken():
        form = request.form
        subject = form['subject']
        name = form['name']
        email = form['email']
        content = form['content']
        sendMail(subject, name, email, content, 'armand@camponovo.xyz')
    return redirect('/admin')


@app.route('/admin/modify/<card_id>')
def modifyCardUI(card_id):
    """
    Html page to modify card
    :param card_id: card id
    :return: html page
    """
    if validConnectionToken():
        creation = creations.find_one({'_id': ObjectId(card_id)})
        creation['image'] = [image.split('/')[-1] for image in creation['image']]
        return render_template('modify-card.html', card=creation)
    else:
        return redirect('/admin')


@app.route('/admin/modify/<card_id>/save', methods=['POST'])
def modifyCard(card_id):
    if validConnectionToken():
        old_creation = creations.find_one({'_id': ObjectId(card_id)})
        result = request.form
        files = result['files']
        filesPaths = []
        for file in files.split(','):
            if file != '':
                filesPaths.append(file)
        if len(files.split(',')) <= 1:
            filesPaths = old_creation['image']
        title = result['title']
        subtitle = result['subtitle']
        content = result['content']
        tags = result['tags'].split(',')
        tags.remove('tout') if 'tout' in tags else ...
        tags.remove(old_creation['type'])
        for tag in tags:
            if isNotTagInDB(tag):
                canBeSearchedTags.insert_one({'name': tag})
        creationType = result['type']
        tags.append(creationType)
        date = datetime.datetime.now().strftime("%d/%m/%Y")
        creations.update_one({'_id': ObjectId(card_id)}, {'$set': {'title': title, 'subtitle': subtitle, 'content': content, 'date': date, 'image': filesPaths, 'tags': tags, 'type': creationType, 'baseurl': '/card/'}})
        return redirect(f'/card/{card_id}')
    return redirect('/admin')


@app.route('/admin/modify/<card_id>/delete', methods=['GET'])
def deleteCard(card_id):
    if validConnectionToken():
        creations.delete_one({'_id': ObjectId(card_id)})
        return redirect(f'/admin/gallery')
    return redirect('/admin')


@app.route('/admin/upload', methods=['POST'])
def uploadFile():
    if validConnectionToken():
        file = request.files.get('file')
        filename = file.filename
        path = "uploads/" + filename
        while os.path.isfile(path):
            filename = "_" + filename
            path = "uploads/" + filename
        file.save(path)
        return "/" + path
    return redirect('/admin')


@app.route('/add-card', methods=['POST'])
def addCard():
    """
    Add a card to database
    :return: html page
    """
    if validConnectionToken():
        result = request.form
        files = result['files']
        filesPaths = []
        for file in files.split(','):
            if file != '':
                filesPaths.append(file)
        title = result['title']
        subtitle = result['subtitle']
        content = result['content']
        tags = result['tags'].split(',')
        tags.remove('tout') if 'tout' in tags else ...
        for tag in tags:
            if isNotTagInDB(tag):
                canBeSearchedTags.insert_one({'name': tag})
        creationType = result['type']
        tags.append(creationType)
        date = datetime.datetime.now().strftime("%d/%m/%Y")
        creations.insert_one({'title': title, 'subtitle': subtitle, 'content': content, 'date': date, 'image': filesPaths, 'tags': tags, 'type': creationType, 'baseurl': '/card/'})
        return redirect('/creations')
    return redirect('/')


@app.errorhandler(Exception)
def serverError(server_error):
    """
    Catch errors and return an error page
    :param server_error: The complete error
    :return: An error page
    """
    return render_template('error.html', error=server_error, page=getRandomErrorPage())


@app.before_first_request
def init():
    """
    Init users, delete and recreate index $text;
    users: all administrators
    tokens: administrators tokens
    :return: None
    """
    global users, tokens
    users = {f"{user['name']}": user['password'] for user in registeredUsers.find()}
    tokens = {f"{user['name']}": user['token'] for user in registeredUsers.find()}
    try:
        creations.drop_index([('title', 'text'), ('subtitle', 'text'), ('content', 'text'), ('tags', 'text')])
    except Exception as error:
        print(f'Failed to drop index $text : {error.__class__.__name__}')
    else:
        print('Successfully deleted and recreated index $text')
    finally:
        creations.create_index([('title', 'text'), ('subtitle', 'text'), ('content', 'text'), ('tags', 'text')])


def replaceIdByTag(dictionary: dict) -> dict:
    """
    Replace value _id by value of tag
    :param dictionary: dictionary to value _id by value of tag
    :return: dict updated
    """
    dictionary['_id'] = dictionary['tag']
    return dictionary


def replaceBaseurl(dictionary: dict, url: str) -> dict:
    """
    Replace value _id by value of tag
    :param dictionary: dictionary to value _id by value of tag
    :param url: value to replace with
    :return: dict updated
    """
    dictionary['baseurl'] = url
    return dictionary


def deleteId(dictionary: dict) -> dict:
    """
    Delete the _id value of the dictionary parameter
    :param dictionary: dictionary to replace _id value
    :return: dict updated
    """
    del dictionary['_id']
    return dictionary


def addCategory(name: str, image: List[str], subtitle: str, tag: str) -> None:
    """
    Add a category of creation
    :param name: name od category
    :param image: image of category
    :param subtitle: subtitle of category
    :param tag: tag of category
    :return: None
    """
    categories.insert_one({'title': name, 'subtitle': subtitle, 'image': image, 'tag': tag, 'baseurl': '/creations?filters='})


def addAdministrator(name, password):
    hashed_password = hashPassword(password, salt(name))
    token = generateRandomToken()
    registeredUsers.insert_one({'name': name, 'password': hashed_password, 'token': token})


def getHref() -> str:
    """
    Get current href
    :return: str href
    """
    return request.path


def tagCanBeSearched() -> List[dict]:
    """
    Return tags can be searched
    :return: collection of Tags that are in the canBeSearchedDB
    """
    return [deleteId(document) for document in canBeSearchedTags.find()]


def getRandomErrorPage() -> Tuple[str, str]:
    """
    Get a random error page html structure
    :return:
    """
    images_texts = [
        ('/static/gif/final/Logo.gif', 'Ah si une fleur perdue...'),
        ('/static/gif/final/en-chantier.gif', 'Oula c\'est un vrai chantier ici !'),
        ('/static/gif/more/sport2.png', 'Allez du badminton pour relativiser !'),
        ('/static/gif/more/art1.png', 'On sort Singertrude et on bosse maintenant !'),
        ('/static/gif/more/engagements1.png', 'J\'avais un truc hyper important à dire mais j\'m\'en souviens plus...'),
        ('/static/assets/mentions-legales.png', ''),
        ('/static/gif/raw/10.png', 'Quel bazar ici ! Cette partie du site est très mal rangée !'),
        ('/static/gif/final/3.gif', 'Abracadabra des paillettes pour s\'occuper !')
    ]
    return random.choice(images_texts)


app.jinja_env.globals.update(href=getHref)
app.jinja_env.globals.update(avaiblesTags=tagCanBeSearched)

if __name__ == '__main__':
    app.run(port=1210, debug=True, host='0.0.0.0')
