import base64
import datetime
import os
import sys
import zipfile
import pymongo
from env_loader import load_env


env = load_env('../.')
MONGO_URL = env.get('MONGO_URL')
args = '?retryWrites=true&w=majority'
client = pymongo.MongoClient(MONGO_URL + args)
creations = client.Ondidine.creations


def get_base64_encoded_image(image_path):
    with open(image_path, "rb") as img_file:
        return base64.b64encode(img_file.read()).decode('utf-8')


def backup():
    for creation in creations.find():
        images = ""
        for image in creation['image']:
            images += f"<img src=\"data:image/png;base64,{get_base64_encoded_image('..' + image)}\">"
        file_content = f"# {creation['title']}\n## {creation['subtitle']}\n\n\n\n{creation['content']}\n{images}\n\n\n---ondine.camponovo.art---\n\ntags:{','.join(creation['tags'])}\ntype:{creation['type']}\nbaseurl:{creation['baseurl']}\nraw:{creation}\n\n---ondine.camponovo.art---"
        open(f"backups/files/{creation['title']}.md", "w+").write(file_content)


def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


def zip_():
    time = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    zipped = zipfile.ZipFile(f'backups/zips/backup_{time}.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('backups/files', zipped)
    zipped.close()
    if '--del' in sys.argv:
        for file in os.listdir('backups/files'):
            os.remove(f'backups/files/{file}')


if __name__ == '__main__':
    if '--zip' in sys.argv:
        backup()
        zip_()
    else:
        backup()
