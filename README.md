# ondine.camponovo.art

Une surprise pour son anniv ! Chutt !

# Deploy:

## Manuellement :
1. Installer les prérequis :<br>
   - python :<br>
       Linux :
        ```shell
       sudo apt install python3 python3-pip
       ```
       Windows :
       Allez sur [python.org](https://python.org) et installez python3
   - librairies python :<br>
       Linux :
       ```shell
       pip install -r requirements.txt
       ```
       Windows:
       ```shell
       python3 -m pip install -r requirements.txt
       ```
2. Copier le `.env.sample` en `.env` et donner l'url de la base de donnée et le mot de passe de noreply@camponvo.art
3. Lancer le script:
```shell
python3 main.py
```

## Avec Docker:
(requis docker)
1. Copier le `.env.sample` en `.env` et donner l'url de la base de donnée et le mot de passe de noreply@camponvo.art
2. Build et lancer le conteneur (ne pas oublier le volume pour les images uploadées et le paramètre `-p 1210:1210` pour les ports)
```shell
docker build -t siteweb . && docker run -v /path/to/upload/folder:/website/uploads -p 1210:1210 siteweb
```
3. (pour un serveur de developpement avec docker, il faut simplement rajouter l'argument `-f Dockerfile.development` à la commande de build)

## Pour la production:
Si le script est lancé manuellement depuis un terminal il est préférable d'utiliser un server gunicorn:
```shell
gunicorn --bind 0.0.0.0:1210 prod:app
```

